import os
from redis import StrictRedis
import settings

#usage:
#put all target words in TXT file in the TARGETPATH(each one per line)
#control.py will read all target words in the txt and finish crawlign automatically


#edit target path here
TARGETPATH = "lists\\targets.txt"


#sometimes there are not enough pages;
#if for the i times the spider has not got pages more than leastnum[i],the spider would retry.
#tips: sometimes Baidu just give too few pages of results to crawl:
#try: set longer interval betwenn starting date and ending date.
#or: use target words that are more frequently used.
leastnum = [60,45,20,20,20,15,15,15,15,10]

targets = [line.strip() for line in open("lists\\targets.txt","r").readlines()]
redis = StrictRedis(host=settings.redisHOST, port=settings.redisPORT, db=settings.redisDB)
for i in targets:
    if (redis.get(i)):
            continue
    else :
        redis.set(i,1)
        print("crawl target = ",i)
        j = 0
        for j in range (10):
            if(int(redis.get(i))<leastnum[j]):
                file = open(i+'.txt','w')
                file.write("")        
                file.close()
                os.system("python main.py -t "+i)
            else :
                break
        if(j==9):
            if(int(redis.get(i))<10):
                file = open("lists\\error.txt","a")
                file.write(i)
                file.write('\n')
                file.close()


print('finished')
