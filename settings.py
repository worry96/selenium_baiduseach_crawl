# -*- coding: utf-8 -*-

# Scrapy settings for sebaidusearch project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'sebaidusearch'
SPIDER_MODULES = ['sebaidusearch.spiders']
NEWSPIDER_MODULE = 'sebaidusearch.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False


#costum settings related to the spider
#default start and end date:
START_DATE = '2018-06-15'
END_DATE = '2018-07-23'
#default number of pages to scratch
PAGE = 100
#default target to search
TARGET = '知乎'
#add path to link with chromedriver
CHROMEPATH = ''


#costum settings related to database
DB = 'newdb'
HOST = 'test'
PORT = 3306,
USER = 'user'
PASSWD = '1234',

#ippool updatedate: 07-16
IPPOOL=[
    {"ipaddr":"6119.136.145.159:808"},
    {"ipaddr":"115.223.214.228:9000"},
    {"ipaddr":"114.101.46.82:63909"},
    {"ipaddr":"182.87.137.133:9000"},
    {"ipaddr":"27.220.125.44:9000"},
    {"ipaddr":"106.5.202.2:9000"},
    {"ipaddr":"121.232.148.93:9000"},
    {"ipaddr":"115.223.194.77:9000"},
    {"ipaddr":"115.223.241.30:9000"},
    {"ipaddr":"101.96.11.74:8090"},
    {"ipaddr":"118.117.136.114:9000"},
    {"ipaddr":"115.218.121.143:9000"},
    {"ipaddr":"114.235.22.226:9000"},
    {"ipaddr":"49.81.125.21"},    
    {"ipaddr":"115.218.120.75:9000"},    
    {"ipaddr":"120.25.203.182:7777"},    
    {"ipaddr":"117.90.252.55:9000"},    
    {"ipaddr":"115.223.229.116:9000"},
    {"ipaddr":"60.18.0.245:80"},
    {"ipaddr":"115.223.192.98:9000"},
]








#choose to hide chrome to improve speed
HIDECHROME = 'FALSE'
# Configure item pipelines:choose pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'sebaidusearch.pipelines.SebaidusearchPipeline': 300,
    }


# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 4

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 0.2
DOWNLOAD_TIMEOUT = 200
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 100
#CONCURRENT_REQUESTS_PER_IP = 100

dont_filter= True

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

#LOG_LEVEL = 'INFO'

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'sebaidusearch.middlewares.SebaidusearchSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'sebaidusearch.middlewares.SebaidusearchSpiderMiddleware': None,
    'sebaidusearch.middlewares.RotateUserAgentMiddleware':400,
    "scrapy.contrib.downloadermiddlewares.redirect.RedirectMiddleware": None,
    "sebaidusearch.middlewares.MyretryMiddleware": 302
#    'sebaidusearch.middlewares.MyproxiesSpiderMiddleware':543,
}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

#settings for redis server
redisHOST = "localhost"
redisPORT = 6379
redisDB = 0









# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
