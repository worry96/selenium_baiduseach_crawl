#!/usr/bin/env python
# -*- coding: utf-8 -*-
from scrapy import cmdline
import argparse
import sys
import os
import settings

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--TARGET', help='set search-target here', dest='TARGET', default='LadyGaga')
parser.add_argument('-s', '--START_DATE', help='define start date here like 2018-01-01', dest='START_DATE',
                    default='2017-01-01')
parser.add_argument('-e', '--END_DATE', help='define end date here like 2018-07-01', dest='END_DATE',
                    default='2018-09-07')
parser.add_argument('-p', '--PAGE',
                    help='define how many pages to crawl(default:76)',
                    dest='PAGE', type=int, default=76)
fsettings = parser.parse_args()

settings.START_DATE = fsettings.START_DATE
settings.END_DATE = fsettings.END_DATE
settings.PAGE = fsettings.PAGE
settings.TARGET = fsettings.TARGET



sys.path.append(os.path.dirname(os.path.abspath(__file__)))
cmdline.execute("scrapy crawl spider".split())
