# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from selenium import webdriver
#from selenium.webdriver.common.by import By
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
#from scrapy.selector import Selector
from sebaidusearch.items import SebaidusearchItem
from time import sleep
#import sys
from redis import StrictRedis
from sebaidusearch import settings


class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['baidu.com']
    start_date = settings.START_DATE
    end_date = settings.END_DATE
    url = 'https://www.baidu.com/s?wd=%s' % settings.TARGET
    page = settings.PAGE
    if (page>76):
        page = 76

#   redis database explaination:
#   key refers to page
#   value refers to condition:
#   (0:not requested; 1:waiting for response; 2:failed but retrying 3:prased successfully)    
    redis = StrictRedis(host=settings.redisHOST, port=settings.redisPORT, db=settings.redisDB)
    for i in range(page):
        redis.set(i+1,0)
        
    hidechrome=settings.HIDECHROME
    
#overwrite __init__ to pass parameters through command line tools 
    #def __init__(self, *args, **kwargs):
        #super(SpiderSpider, self).__init__(self)
        #self.start_date = kwargs['START_DATE']
        #self.end_date = kwargs['END_DATE']
        #self.url = 'https://www.baidu.com/s?wd=%s' % kwargs['TARGET']
        #self.page = kwargs['PAGE']
        #if(self.page>76):
        #    self.page = 76
        #if (len(kwargs['CHROMEPATH'])!=0):
        #    sys.path.append(kwargs['CHROMEPATH'])
        #self.hidechrome = kwargs['HIDECHROME']
        
        
#pass parameters start_date,end_date,target,page,chrome path
#    @classmethod
#    def from_crawler(cls, crawler, *args, **kwargs):
#        kwargs['START_DATE'] = crawler.settings.get('START_DATE')
#        kwargs['END_DATE'] = crawler.settings.get('END_DATE')
#        kwargs['TARGET'] = crawler.settings.get('TARGET')
#        kwargs['PAGE'] = crawler.settings.get('PAGE')
#        kwargs['CHROMEPATH'] = crawler.settings.get('CHROMEPATH')
#        kwargs['HIDECHROME'] = crawler.settings.get('HIDECHROME')
#        spider = super(SpiderSpider, cls).from_crawler(crawler, *args, **kwargs)
#        return spider

    def start_requests(self):
        yield Request(url=self.url,callback=self.parse_content)
    
    def parse_content(self,response):
        url=self.url
        chrome_options = webdriver.ChromeOptions()
        print(self.start_date,self.url)
        if (self.hidechrome=='TRUE'):
            chrome_options.set_headless()
        driver = webdriver.Chrome(chrome_options = chrome_options)  
        driver.get(url)
        sleep(1)
        if response.xpath('//*[@id="container"]/div[2]/div/div[2]/div/i'):
            module=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[2]/div/i')
            module.click() 
            sleep(1)
            locator=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[1]/i')
            ActionChains(driver).click(locator)
#                print(driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]').text)
            if driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[3]').text =='时间不限':
                locator2=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[3]')
                locator2.click()
                locator3=driver.find_element_by_xpath('//*[@id="c-tips-container"]/div[2]/div/div/ul/li[6]/a')
                input1=driver.find_element_by_name('st') #start date
                input2=driver.find_element_by_name('et') #end date
                input1.clear()
                ActionChains(driver).move_to_element(input1).send_keys(self.start_date).perform()
                input2.clear()
                ActionChains(driver).move_to_element(input2).send_keys(self.end_date).perform()
                locator3.click()
                sleep(1) 
                yield Request(url=driver.current_url,callback=lambda response, page=1:self.parse_page(response,page),dont_filter= True)
                driver.close()                    
            
            elif driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]').text=='时间不限':
                locator2=driver.find_element_by_xpath('//*[@id="container"]/div[2]/div/div[1]/span[2]')
                locator2.click()
                locator3=driver.find_element_by_xpath('//*[@id="c-tips-container"]/div[1]/div/div/ul/li[6]/a')
                input1=driver.find_element_by_name('st') #start date 
                input1.clear()
                input2=driver.find_element_by_name('et') #end date
                ActionChains(driver).move_to_element(input1).send_keys(self.start_date).perform()
                input2.clear()
                ActionChains(driver).move_to_element(input2).send_keys(self.end_date).perform()
                locator3.click()
                sleep(1)
                yield Request(url=driver.current_url,callback=lambda response, page=1:self.parse_page(response,page),dont_filter= True)
                driver.close()
    
    def parse_page(self,response,page):
        if(page<=self.page):
            allinfo = response.xpath('//div[@srcid="1599"]')
            for i in range(len(allinfo)):
                item=SebaidusearchItem()
                time = allinfo[i].xpath('div/span[@class=" newTimeFactor_before_abs m"]/text()').extract()
                if(len(time)==0):
                    time = allinfo[i].xpath('div/div/div/span[@class=" newTimeFactor_before_abs m"]/text()').extract()
                titlepart = allinfo[i].xpath('h3/a')
                title = titlepart.xpath('string()').extract()
                if(len(title)==0):
                    continue
                if(len(time)==0):
                    continue
                timesplited = time[0].split()[0]
                print(timesplited,title[0])
                item['time'] = timesplited
                item['title'] = title[0]
                yield item
            self.redis.set(page,3)
        else: return

        pagepart = response.xpath('//*[@id="page"]/a[not(contains(text(),"页"))]')
        pageurls = pagepart.xpath('//strong/following-sibling::*/@href').extract()
        for i in range(4):
            if(page+i+1>self.page):
                return
            if(len(pageurls)<=i):
                return
            if(int(self.redis.get(page+i+1))!=0):
                return
            self.redis.set(page+i+1,1)
            newurl = 'http://www.baidu.com'+ pageurls[i]
            print(page+i+1,'\n',newurl,'\n')
            yield Request(url=newurl,callback=lambda response, p=page+i+1:self.parse_page(response,p),dont_filter= True)
        

