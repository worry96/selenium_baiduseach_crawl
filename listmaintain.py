from redis import StrictRedis
from sebaidusearch import settings


#includes functions helpful to administer lists
error = [line.strip() for line in open("lists\\error.txt","r").readlines()]
done = [line.strip() for line in open("lists\\done.txt","r").readlines()]
rawlist = [line.strip() for line in open("lists\\rawlist.txt","r").readlines()]
redis = StrictRedis(host=settings.redisHOST, port=settings.redisPORT, db=settings.redisDB)


def reset:
#   Words in "error" list is recorded as "crawled" in redis service,and they will be ignored.
#   If you want to retry with the target words in "error" list,pls call this function first.

    for i in error:
        redis.delete(i)

        
def generatetargets:
#   Ignore words in rawlist that crawled then put others in targets.list
targets = open("lists\\targets.txt","a")
    for i in rawlist:
        if (i not in done):
            targets.write("i")
            targets.write("")
