# selenium_baiduseach_crawl
crawl time and title from baidusearch on timeline with selenium and scrapy
______________________________________________________________ 
Requirements:
1.python3 on Anaconda
2.Scrapy 1.4
3.Selenium 3.12
4.Chromedrive v2.4
5.Redis service
___________________________________________________________________________
Before using there are following things to do:
1.create scrapy.cfg and put main.py in the same path.
2.start redis service and set correct settings related to redis in settings.py
3.put chromedrive.exe(v2.40) in the same path

____________________________________________________________________________
Usage:
1.main.py:
run single spider to crawl and support editting frequentlt used parameters through cmdline
see more:
python main.py -h
2.control.py:
run spiders to crawl a list of target words.

____________________________________________________________________________
Tips:
1.To improve speed,you can run several control.py at the same time(they will share one target words list)
2.Default settings writen in the main.py will overwrite those in settings.py
3.Sometimes spiders finish crawling without getting enough results.
These target words would be recorded in error.txt.
Possible reason: Network;the website did not show enough pages;
advice: retry with words in error.txt(see listmaintain.py--reset);run less spiders at the same time;
set larger DOWNLOAD_DELAY in settings.py


____________________________________________________________________________
Imporvements:
1.Imporved integrity of result
2.Imporved speed
3.Imporved stability
4.Can Automatically crwalling a list of words

